# VI Curso Livre de Cannabis Medicinal 

Compartilhando o link das aulas do VI Curso Livre de Cannabis Medicinal (UNIFESP / MOVRECAM / INSTITUTO JUREMA / SBEC)

## Aulas

- [1ª aula: Etnofarmacologia - Professora Eliana Rodrigues](https://www.youtube.com/watch?v=f4ncGtGd7K0&list=PLjmLIhmqbdWnsmc-894K_80eI4kRuPs9P&index=4)
- [2ª aula - Cidadania e Saúde - Prof. Patrícia Milani](https://www.youtube.com/watch?v=8XYvigi7b04&list=PLjmLIhmqbdWnsmc-894K_80eI4kRuPs9P&index=5)
- [3ª Aula: Política e Legislação / Direitos à Saúde / Política de Drogas e suas consequências sociais](https://www.youtube.com/watch?v=KAoU6-CdlP4&list=PLjmLIhmqbdWnsmc-894K_80eI4kRuPs9P&index=6)
- [4ª aula - Farmácia Viva / Prof. Jaqueline Guimarães - Sus / Dra. Úrsula Catarino](https://www.youtube.com/watch?v=0CL-W8s4t-E&list=PLjmLIhmqbdWnsmc-894K_80eI4kRuPs9P&index=7)
- [5ª aula - Associativismo - Júlio Américo, Cidinha Carvalho e Fernando Profeta](https://www.youtube.com/watch?v=6waQ41uEPNM)
- [6ª Aula do VI Curso: Pesquisa com Cannabis - Dr Shiavone e Dr. Fabrício Pamplona](https://www.youtube.com/watch?v=JKthzR7J6Qw&list=PLjmLIhmqbdWnsmc-894K_80eI4kRuPs9P&index=8)
- [7ª aula - Técnicas de Cultivo de Cannabis - Profs. Danilo Massuela, Henrique Araújo e Robert Gandra](https://www.youtube.com/watch?v=4NXqRsMiS8g&list=PLjmLIhmqbdWnsmc-894K_80eI4kRuPs9P&index=10)
- [8ª aula - Mercado Canábico - Professores Maria Eugênia e Thiago](https://www.youtube.com/watch?v=OtGSOj21gvQ&list=PLjmLIhmqbdWnsmc-894K_80eI4kRuPs9P&index=11)
- [9ª Aula - Farmacologia e Métodos de Extração - Prof. Renata Monteiro](https://www.youtube.com/watch?v=IpICFMYO-bc&list=PLjmLIhmqbdWnsmc-894K_80eI4kRuPs9P&index=9)
- [10ª aula - Sistema Endocanabinóide - Prof. Paulo de Morais](https://www.youtube.com/watch?v=p5eeROgxng4)
- [11ª aula - Autismo e Cannabis Medicinal - Dra. Eliane Nunes](https://www.youtube.com/watch?v=2h4Hc0LqSQ8&list=PLjmLIhmqbdWnsmc-894K_80eI4kRuPs9P&index=12)
- [12ª Aula - Câncer - Dra. Paula Toledo / Revolução Canabinóide - Dr. Sidarta Ribeiro](https://www.youtube.com/watch?v=HOYas8rxqlo&list=PLjmLIhmqbdWnsmc-894K_80eI4kRuPs9P&index=13)
- [13ª Aula - Cannabis e Dor - Dra. Carolina Nocetti](https://www.youtube.com/watch?v=k_lAZDCX-Dw&list=PLjmLIhmqbdWnsmc-894K_80eI4kRuPs9P&index=15)
- [14ª aula - A Cannabis, Saúde Mental e Ansiedade - Prof. Dr. Paulo Morais e Dr Eduardo Perin](https://www.youtube.com/watch?v=5WgsWqoWdeU&list=PLjmLIhmqbdWnsmc-894K_80eI4kRuPs9P&index=16)
- [15ª Aula: Cannabis e Epilepsia - Dr. Andrade - Cannabis, Alzheimer e Parkinson, Dra. Denise Pedra](https://www.youtube.com/watch?v=9tauTWy8eFg&list=PLjmLIhmqbdWnsmc-894K_80eI4kRuPs9P&index=17)
- [16ª aula - Uso da Cannabis na Odontologia](https://www.youtube.com/watch?v=pP8cRKXar8g&list=PLjmLIhmqbdWnsmc-894K_80eI4kRuPs9P&index=18)
- [17ª aula - Cannabis na Veterinária - Dra Aline Mendes](https://www.youtube.com/watch?v=naPJhvS2EGE&list=PLjmLIhmqbdWnsmc-894K_80eI4kRuPs9P&index=19)
- [18ª Aula - Cannabis nas dependências (Prof Paulo de Morais) - Na Saúde da Mulher (Dr. Hélio Mororó)](https://www.youtube.com/watch?v=ExqJWzKiT7M&list=PLjmLIhmqbdWnsmc-894K_80eI4kRuPs9P&index=20)
- [19ª aula: Cannabis: apresentações farmacêuticas, cosmética e culinária - Renata Monteiro](https://www.youtube.com/watch?v=WlkvD1JTM-U&list=PLjmLIhmqbdWnsmc-894K_80eI4kRuPs9P&index=21)







